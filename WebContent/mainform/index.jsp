<!DOCTYPE html>
<html lang="en">
<head>
	<%@ include file='head.jsp'%>
	<link href="css/simpleLightbox.css">
	<style>
		.zoom {
			display: inline-block;
			position: relative;
			border-radius: 0.5rem;
			margin: 0.5rem 0;
		}
		
		.zoom img {
			display: block;
			width: 100%;
			height: auto;
			border-radius: 0.5rem;
			position: relative;
		}
		
		.zoom img::selection {
			background-color: transparent;
		}
		
		.zoom div {
			background-color: rgba(0, 0, 0, 0.5);
			color: #fff;
			font-weight: bold;
			position: absolute;
			z-index: 1;
			width: 100%;
			padding: 0.5rem 1rem;
		}
	</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="card-columns cols-2">
					<div class="card">
						<div class="card-header">
							Claim Submissions
							<div class="card-header-actions">
								<a class="card-header-action" href="http://www.chartjs.org" target="_blank"> <small class="text-muted">docs</small>
								</a>
							</div>
						</div>
						<div class="card-body">
							<div class="chart-wrapper">
								<canvas id="canvas-1"></canvas>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							Claim Status
							<div class="card-header-actions">
								<a class="card-header-action" href="http://www.chartjs.org" target="_blank"> <small class="text-muted">docs</small>
								</a>
							</div>
						</div>
						<div class="card-body">
							<div class="chart-wrapper">
								<canvas id="canvas-3"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<!-- Plugins and scripts required by this view-->
	<script src="mainform/plugins/Chart.min.js"></script>
	<script src="mainform/plugins/custom-tooltips.min.js"></script>
	<script src="mainform/js/main.js"></script>
	<script src="mainform/js/jquery.zoom.min.js"></script>
	<script>
	$(document).ready(function() {
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify({"APIUrl":"/claim/get-claim-count", "method":"GET"}),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
		}).done(function (data) {
			console.log(data);
			if(data.error_schema.error_code == "ERR-00-000"){
				var inprogress_array = new Array(12).fill(0);
				var paid_array = new Array(12).fill(0);
				var pending_array = new Array(12).fill(0);
				var rejected_array = new Array(12).fill(0);
				
				var max_number_array = new Array();
				
				$.each(data.output_schema.items, function(key, value) {					
					if(value.claim_status == "INPROGRESS"){
						inprogress_array[value.claim_month - 1] = parseInt(value.claim_count);
						//inprogress_array.push({"month":value.claim_month, "count":value.claim_count, "status":value.claim_status});
					}
					
					if(value.claim_status == "PAID"){
						paid_array[value.claim_month - 1] = parseInt(value.claim_count);
						//paid_array.push({"month":value.claim_month, "count":value.claim_count, "status":value.claim_status});
					}
					
					if(value.claim_status == "PENDING"){
						pending_array[value.claim_month - 1] = parseInt(value.claim_count);
						//pending_array.push({"month":value.claim_month, "count":value.claim_count, "status":value.claim_status});
					}
					
					if(value.claim_status == "REJECTED"){
						rejected_array[value.claim_month - 1] = parseInt(value.claim_count);
						//rejected_array.push({"month":value.claim_month, "count":value.claim_count, "status":value.claim_status});
					}
				});
				
				var lineChart = new Chart($('#canvas-1'), {
					type: 'line',
					data: {
						labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
						datasets: [{
							label: 'In Progress',
							backgroundColor: 'rgba(0, 134, 255, 0.2)',
							borderColor: 'rgba(0, 134, 255, 1)',
							pointBackgroundColor: 'rgba(0, 134, 255, 1)',
							pointBorderColor: '#fff',
							data: inprogress_array
						}, {
							label: 'Paid',
							backgroundColor: 'rgba(0, 255, 61, 0.2)',
							borderColor: 'rgba(0, 255, 61, 1)',
							pointBackgroundColor: 'rgba(0, 255, 61, 1)',
							pointBorderColor: '#fff',
							data: paid_array
						}, {
							label: 'Pending',
							backgroundColor: 'rgba(255, 255, 124, 0.2)',
							borderColor: 'rgba(255, 255, 124, 1)',
							pointBackgroundColor: 'rgba(255, 255, 124, 1)',
							pointBorderColor: '#fff',
							data: pending_array
						}, {
							label: 'Rejected',
							backgroundColor: 'rgba(255, 37, 0, 0.2)',
							borderColor: 'rgba(255, 37, 0, 1)',
							pointBackgroundColor: 'rgba(255, 37, 0, 1)',
							pointBorderColor: '#fff',
							data: rejected_array
						}]
					},
					options: {
						responsive: true
					}
				});
				
				var current_month = (new Date).getMonth();
				console.log();
				
				var doughnutChart = new Chart($('#canvas-3'), {
					type: 'doughnut',
					data: {
						labels: ['IN PROGRESS', 'PAID', 'PENDING', 'REJECTED'],
						datasets: [{
							data: [
								inprogress_array[current_month], 
								paid_array[current_month], 
								pending_array[current_month], 
								rejected_array[current_month]
							],
							backgroundColor: ['rgba(0, 134, 255, 0.8)', 'rgba(0, 255, 61, 0.8)', 'rgba(255, 255, 124, 0.8)', 'rgba(255, 37, 0, 0.8)'],
							hoverBackgroundColor: ['rgba(0, 134, 255, 0.8)', 'rgba(0, 255, 61, 0.8)', 'rgba(255, 255, 124, 0.8)', 'rgba(255, 37, 0, 0.8)']
						}]
					},
					options: {
						responsive: true
					}
				});
				
			}
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
		
	});
	</script>
</body>
</html>
