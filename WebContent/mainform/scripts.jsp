


<!-- CoreUI and necessary plugins-->
<script src="mainform/plugins/jquery-3.4.1.min.js"></script>
<script src="mainform/plugins/popper.min.js"></script>
<script src="mainform/plugins/bootstrap.min.js"></script>

<script src="mainform/plugins/pace.min.js"></script>
<script src="mainform/plugins/perfect-scrollbar.min.js"></script>
<script src="mainform/plugins/coreui.min.js"></script>
<script src="mainform/plugins/sweetalert2.all.min.js"></script>

<script src="mainform/plugins/datatables/datatables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<script src="mainform/plugins/jquery.validate.min.js"></script>
<script src="mainform/plugins/additional-methods.min.js"></script>

<script>
	const protocol = window.location.protocol;
	const environment = window.location.host;
	const url_local_root = protocol + "//" + environment ;
	const url_local = url_local_root + "/BNILifeService";
// 	const url_local_web = url_local_root + "/BNILifeWeb";
	const url_local_web = url_local_root + "${pageContext.request.contextPath}";

	function getPageName() {
		var index = window.location.href.lastIndexOf("/") + 1,
			filenameWithExtension = window.location.href.substr(index),
			filename = filenameWithExtension.split(".")[0];
		return filename;
	}
	
	function profileOK() {
		swal.fire('Success', 'Profile Has Been Updated', 'success')
	}
	
	function userLogout() {
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Log Out'
		}).then((result) => {
			if (result.value) {
				window.location.href = url_local_web+'/logout';
			}
		});
	}
</script>