<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="customization container-fluid">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-4 col-12">
						<h5>Administrator</h5>
						<hr>
						<textarea class="form-control">user1, user2, user3</textarea>
						<br>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Claim</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Members</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Providers</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Customization</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Contents</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Settings</div>
						</div>
						<hr>
						<button class="btn btn-primary">Save Changes</button>
					</div>
					<div class="col-md-4 col-12">
						<h5>Moderator</h5>
						<hr>
						<textarea class="form-control">user4, user5, user6</textarea>
						<br>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Claim</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Members</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Providers</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Customization</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Contents</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox"> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Settings</div>
						</div>
						<hr>
						<button class="btn btn-primary">Save Changes</button>
					</div>
					<div class="col-md-4 col-12">
						<h5>Member</h5>
						<hr>
						<textarea class="form-control">user7, user8, user9</textarea>
						<br>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Claim</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Members</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox" checked=""> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Providers</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox"> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Customization</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox"> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Contents</div>
						</div>
						<div class="user-switch">
							<label class="switch switch-label switch-pill switch-outline-primary-alt"> <input class="switch-input"
								type="checkbox"> <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
							</label>
							<div class="user-role-text ml-2">Settings</div>
						</div>
						<hr>
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</div>
		</div>
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<!-- Plugins and scripts required by this view-->
	<script src="mainform/plugins/Chart.min.js"></script>
	<script src="js/charts.js"></script>
	<script src="mainform/plugins/custom-tooltips.min.js"></script>
	<script src="js/main.js"></script>
	<!-- Pagination -->
	<script src="js/jquery.jqpagination.min.js"></script>
	<script>
		$(document).ready(function() {

			$('.pagination').jqPagination({
			link_string : '/?page={page_number}',
			max_page : 40,
			paged : function(page) {
				$('.log').prepend('<li>Requested page ' + page + '</li>');
			}
			});
			$('.show-log').click(function(event) {
				event.preventDefault();
				$('.log').slideToggle();
			});

		});
	</script>
</body>
</html>
