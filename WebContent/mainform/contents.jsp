<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="row justify-content-center">
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">2.345</div>
								<small class="text-muted text-uppercase font-weight-bold">Clinic</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">1.243</div>
								<small class="text-muted text-uppercase font-weight-bold">Hospital</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">431</div>
								<small class="text-muted text-uppercase font-weight-bold">Laboratorium</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">987</div>
								<small class="text-muted text-uppercase font-weight-bold">Optic</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-12 mx-auto">
						<div class="searchbar input-group input-group-lg mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-search"></i></span>
							</div>
							<input type="text" class="form-control" placeholder="Search Provider" aria-label="Search Provider"
								aria-describedby="basic-addon2">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="row pt-3">
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#claimModal">
							<div class="claim-num">RS Lorem Ipsum</div>
							<div class="claim-sum">
								<div class="claim-sum-name">Clinic</div>
								<div>Jakarta Pusat, DKI Jakarta</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="gigantic pagination justify-content-center">
							<a href="#" class="first" data-action="first"><i class="fas fa-angle-double-left"></i></a> <a href="#"
								class="previous" data-action="previous"><i class="fas fa-angle-left"></i></a> <input type="text" readonly /> <a
								href="#" class="next" data-action="next"><i class="fas fa-angle-right"></i></a> <a href="#" class="last"
								data-action="last"><i class="fas fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		</main>
		<!-- Modal -->
		<div class="modal fade" id="claimModal" tabindex="-1" role="dialog" aria-labelledby="claimModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title claim-num" id="claimModalLabel">#1234567890</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">...</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<!-- Plugins and scripts required by this view-->
	<script src="mainform/plugins/Chart.min.js"></script>
	<script src="js/charts.js"></script>
	<script src="mainform/plugins/custom-tooltips.min.js"></script>
	<script src="js/main.js"></script>
	<!-- Pagination -->
	<script src="js/jquery.jqpagination.min.js"></script>
	<script>
		$(document).ready(function() {

			$('.pagination').jqPagination({
			link_string : '/?page={page_number}',
			max_page : 40,
			paged : function(page) {
				$('.log').prepend('<li>Requested page ' + page + '</li>');
			}
			});
			$('.show-log').click(function(event) {
				event.preventDefault();
				$('.log').slideToggle();
			});

		});
	</script>
</body>
</html>
