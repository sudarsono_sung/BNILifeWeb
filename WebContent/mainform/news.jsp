<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link href="mainform/css/quill.snow.css" rel="stylesheet">
<style>
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 60%;
}

#sortable li {
	margin: 0 3px 3px 3px;
	padding: 0.4em;
	padding-left: 1.5em;
	font-size: 1.4em;
	height: 18px;
}

#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<div class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="search-container row">
						<div class="col-lg-3 col-md-3 col-12">
							<button class="btn btn-block btn-warning" type="button" id="ShowAddDoc">
								<i class="fa fa-plus-circle"></i> Create Post
							</button>
						</div>
						<div class="col-lg-3 col-md-3 col-12"></div>
						<div class="col-lg-3 col-md-3 col-12"></div>
					</div>
					<!-- SEARCH-CONTAINER -->
					<div class="animated fadeIn">
						<div class="row dragdrop"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="addDoc" tabindex="-1" role="dialog" aria-labelledby="addDocLabel" aria-hidden="true"
			data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="addDocLabel">Create New Post</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id='addDocForm'>
							<input type="hidden" id="news_id" readonly> <input type="hidden" id="submit_type" readonly>
							<div class="card-body">
								<div class="form-group">
									<label for="news_link">File</label>
									<div class="custom-file" style="margin-bottom: 10px">
										<input type="file" class="custom-file-input " id="customFile"> <label class="custom-file-label"
											for="customFile">Choose file</label>
									</div>
								</div>
								<div class="form-group">
									<label for="news_link">News Link</label> <input type="text" class="form-control" id="news_link" placeholder="Link">
								</div>
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#add-ID" role="tab"
										aria-controls="Indonesia">Indonesia</a></li>
									<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#add-EN" role="tab" aria-controls="English">English</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="add-ID" role="tabpanel">
										<input type="text" id="title_id" class="form-control mb-3" placeholder="Title" required>
										<div id="title_id_validation"></div>
										<div id="editor_id"></div>
										<div id="editor_id_validation"></div>
									</div>
									<div class="tab-pane" id="add-EN" role="tabpanel">
										<input type="text" id="title_en" class="form-control mb-3" placeholder="Title" required>
										<div id="title_en_validation"></div>
										<div id="editor_en"></div>
										<div id="editor_en_validation"></div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" id="save_news">Save changes</button>
					</div>
				</div>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script src="mainform/js/jquery-ui.min.js"></script>
	<script src="mainform/plugins/quill.min.js"></script>
	<script type="text/javascript">
	var toolbarOptions = [
		['bold', 'italic', 'underline', 'strike'],
		['blockquote', 'code-block'], 
		[{'header': 1},{'header': 2}],
		[{'list': 'ordered'},{'list': 'bullet'}],
		[{'script': 'sub'},{'script': 'super'}],
		[{'indent': '-1'}, {'indent': '+1'}],
		[{'direction': 'rtl'}],
		[{'size': ['small', false, 'large', 'huge']}],
		[{'header': [1, 2, 3, 4, 5, 6, false]}],
		[{'color': []}, {'background': []}],
		[{'font': []}],
		[{'align': []}],
		['clean']
	];
	
	var quill_id = new Quill('#editor_id', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	var quill_en = new Quill('#editor_en', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	function update_function(ID){
		$("#submit_type").val("update");
		var input_parameter = {};
		input_parameter.APIUrl = "/news/get-news?id="+ID;
		input_parameter.method = "GET";
		
		console.log(input_parameter);
		
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify(input_parameter),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
			beforeSend: function() {},
		}).done(function (data) {
			console.log(data);
			var data_selected = data.output_schema.items[0];
			$('#news_id').val(data_selected.news_id);
			$('#news_link').val(data_selected.news_link);
			$('#title_id').val(data_selected.news_title_id);
			$('#title_en').val(data_selected.news_title_en);
			
			quill_id.root.innerHTML = data_selected.news_content_id;
			quill_en.root.innerHTML = data_selected.news_content_en;
			
			$('#addDoc').modal('show');
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
	}
	
	function DeleteNews(ID) {
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete News'
		}).then((result) => {
			if (result.value) {
				var input_parameter = {};
				input_parameter.APIUrl = "/news/delete-news?id="+ID;
				input_parameter.method = "GET";
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					data: JSON.stringify(input_parameter),
					dataType: "json",
					headers: {
						'Content-Type': 'application/json'
					},
					beforeSend: function() {
					},
				}).done(function (data) {
					console.log(data);
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							type: 'success',
							title: 'Alert',
							text: "Delete success"
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: "Delete failed"
						});
					}
					drawCards();
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: "Delete error"
					});
				});
				
			}
		});
		
	}
	
	function drawCards(ID){
		var input_parameter = {};
		input_parameter.APIUrl = "/news/get-news";
		input_parameter.method = "GET";
		if(ID != null){
			input_parameter.news_id = ID;
		}
		
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify(input_parameter),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
			beforeSend: function() {},
		}).done(function (data) {
			console.log(data);
			if(typeof data.output_schema != "undefined"){
				var title_array = [];
				var body_array = [];
				$.each(data.output_schema.items, function (index, value) {
					title_array.push({news_id: value.news_id, news_title_en: value.news_title_en, news_title_id: value.news_title_id });
					body_array.push({news_id: value.news_id, news_content_en: value.news_content_en, news_content_id: value.news_content_id });
				});
				printTemplate(title_array, body_array);
			}
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
	}
	
	function printTemplate(title_array, body_array) {
		$('.dragdrop').empty();
		var title_array_string = "";
		$.each(title_array, function (index, value) {
			if(index == 0){
				var active_string_body = 'active show';
			}
			
			title_array_string += '<a class="list-group-item list-group-item-action '+active_string_body+'" id="content-list-'+index+'" data-toggle="tab" href="#content-'+index+'" role="tab" aria-controls="content-home-'+index+'" aria-selected="true">';
			title_array_string += '<div class="row h-100">'
			title_array_string += '<div class="col-lg-10">'+value.news_title_en+' <br><small>'+value.news_title_id+"</div>";
			title_array_string += '<div class="col-lg-2 my-auto">';
			title_array_string += '<button type="button" class="btn btn-danger btn-sm button_delete" style="width: 35px;" onclick="DeleteNews('+value.news_id+')" ><i class="fa fa-trash-o"></i></button>';
			title_array_string += '</div>';
			title_array_string += '</div>'
			title_array_string += '</small> </a>';
			
		});
		
		var body_array_string = "";
		$.each(body_array, function (index, value) {
			if(index == 0){
				var active_string_body = 'active show';
			}
			body_array_string += 
				'<div class="tab-pane fade '+active_string_body+' " id="content-'+index+'" role="tabpanel" aria-labelledby="content-'+index+'">' + 
					'<ul class="nav nav-tabs" role="tablist">' + 
						'<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#content-ID-'+index+'" role="tab" aria-controls="Bahasa">Bahasa</a></li>' + 
						'<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#content-EN-'+index+'" role="tab" aria-controls="English">English</a></li>' + 
					'</ul>' + 
					'<div class="tab-content">' + 
						'<div class="tab-pane active" id="content-ID-'+index+'" role="tabpanel">' + 
							value.news_content_id + 
							'<button type="button" class="btn btn-secondary" data-toggle="modal" onclick=update_function('+value.news_id+')><i class="fa fa-edit mr-2"></i>Edit</button>' + 
						'</div>' + 
						'<div class="tab-pane" id="content-EN-'+index+'" role="tabpanel">' + 
							value.news_content_en + 
							'<button type="button" class="btn btn-secondary" data-toggle="modal" onclick=update_function('+value.news_id+')><i class="fa fa-edit mr-2"></i>Edit</button>' + 
						'</div>' + 
					'</div>' + 
				'</div>' ;
		});
		
		var template = 
			'<div class="col-md-4 content_title_div ">' + 
				'<div class="list-group" id="title-list-tab" role="tablist">' + 
				title_array_string + 
				'</div>' + 
			'</div>' + 
			'<div class="col-8 content_body_div ">' + 
				'<div class="tab-content" id="nav-tabContent">' + 
				body_array_string + 
				'</div>' + 
			'</div>';
		$('.dragdrop').append(template);
		
	}
	
	$(document).ready(function() {
		var news_id = null;
		
		drawCards();
		
		$('#addDoc').on('hidden.bs.modal', function() { 
			$('#addDocForm')[0].reset();
			quill_id.setContents([]);
			quill_en.setContents([]);
		});
		
		$('#ShowAddDoc').on("click", function() {
			$("#submit_type").val("new");
			$('#addDoc').modal('show');
		});
		
		$('#save_news').on("click", function() {
			var input_parameter = {};
			input_parameter.news_title_id = $('#title_id').val();
			input_parameter.news_title_en = $('#title_en').val();
			input_parameter.news_content_id = quill_id.root.innerHTML;
			input_parameter.news_content_en = quill_en.root.innerHTML;
			input_parameter.news_link = $('#news_link').val();
			
			if(input_parameter.news_title_id.length > 0 ){
				$('#title_id_validation').html('<div class="valid-feedback">Input valid.</div>');
			}
			else{
				$('#title_id_validation').html('<div class="invalid-feedback">Input invalid.</div>');
			}
			
			if(input_parameter.news_title_en.length > 0 ){
				$('#title_en_validation').html('<div class="valid-feedback">Input valid.</div>');
			}
			else{
				$('#title_en_validation').html('<div class="invalid-feedback">Input invalid.</div>');
			}
			
			if(!quill_id.editor.isBlank()){
				$('#editor_id_validation').html('<div class="valid-feedback">Input valid.</div>');
			}
			else{
				$('#editor_id_validation').html('<div class="invalid-feedback">Input invalid.</div>');
			}
			
			if(!quill_en.editor.isBlank()){
				$('#editor_en_validation').html('<div class="valid-feedback">Input valid.</div>');
			}
			else{
				$('#editor_en_validation').html('<div class="invalid-feedback">Input invalid.</div>');
			}
			
			if(input_parameter.news_title_id.length > 0 && input_parameter.news_title_en.length > 0 && !quill_id.editor.isBlank() && !quill_en.editor.isBlank() ){
				var url = "";
				var message_type_success = "";
				var message_type_failed = "";
				if($("#submit_type").val() == 'new'){
					message_type_success = "Insert News berhasil.";
					message_type_failed = "Insert News gagal.";
				}
				else{
					message_type_success = "Update News berhasil.";
					message_type_failed = "Update News gagal.";
					
					input_parameter.news_id = $('#news_id').val();
				}
				
				url = url_local_web+"/APICall";
				input_parameter.APIUrl = "/news/insert-update-news";
				input_parameter.method = "POST";
				
				var formData = new FormData();
				formData.append('json', JSON.stringify(input_parameter));
				if (($("#customFile"))[0].files.length > 0) {
					formData.append('file', $('#customFile')[0].files[0]);
				}
				
				$.ajax({
					url : url,
					type: 'POST',
					enctype: 'multipart/form-data',
					cache: false,
					contentType: false,
					processData: false,
					data: formData,
					dataType: "json",
					beforeSend: function() {
					},
				}).done(function (data) {
// 					console.log(data);
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							type: 'success',
							title: 'Alert',
							text: message_type_success
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					}
					drawCards();
					$('#addDoc').modal('hide');
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: message_type_failed
					});
				});
			}
		});
		
	});
	</script>
</body>
</html>
