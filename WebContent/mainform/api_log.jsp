<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>

<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">

<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<table class="table table-striped table-bordered sortable" id="main_table">
						<thead>
							<tr>
								<th>API Log ID</th>
								<th>API Source</th>
								<th>API Name</th>
								<th>API Status</th>
								<th>Function Name</th>
								<th>API Input</th>
								<th>API Output</th>
								<th>Exception</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			searchDelay: 500,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			serverSide: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function (param) {
					var pageNumber = $('#main_table').DataTable().page.info().page + 1;
					var pageSize = $('#main_table').DataTable().page.info().length;
					var input_parameter = {};
					var searchstring = "";
					if (param.search.value.trim()) {
						searchstring = '&search='+encodeURIComponent(param.search.value);
					}
					
					input_parameter.APIUrl = "/cms/get-api-log-list?pageNumber="+pageNumber+"&pageSize="+pageSize+'&draw='+param.draw+searchstring;
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": "output_schema.data"
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},    
			columns: [
				{"data": "api_log_id"},//0
				{"data": "api_source"},//1
				{"data": "api_name"},//2
				{"data": "api_status"},//3
				{"data": "function_name"},//4
				{"data": "api_input"},//5
				{"data": "api_output"},//6
				{"data": "exception"}//7
			],
			columnDefs: [
				{
					targets: [1, 2, 3, 4],
					searchable: true,
					sortable: false,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [5, 6, 7],
					searchable: false,
					sortable: false,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [0],
					searchable: false,
					sortable: false,
					visible: false,
					defaultContent: ""
				}
			]
		}).on('xhr.dt', function (e, settings, json, xhr) {
			json.recordsTotal = json.output_schema.recordsTotal;
		    json.recordsFiltered = json.output_schema.recordsFiltered;
		    json.draw = json.output_schema.draw;
		    json.data = json.output_schema.data;
		});
		
	});
	</script>
</body>
</html>
