	<header class="app-header navbar">
		<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
			<span class="navbar-toggler-icon"></span>
		</button>
		<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="${pageContext.request.contextPath}/dashboard">
			<img class="navbar-brand-full" src="mainform/img/brand/logo.png" width="89" height="25" alt="BNI Life Mobile Logo">
		</a>
		<ul class="nav navbar-nav ml-auto">
			<li class="nav-item dropdown">
				<a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="d-none d-md-inline">${user_id}</span>
					<img class="img-avatar" src="mainform/img/avatar.png" alt="${user_id}">
				</a>
				<div class="dropdown-menu dropdown-menu-right">
<!-- 					<a class="dropdown-item" href="#" data-toggle="modal" data-target="#userProfile"> -->
<!-- 						<i class="fa fa-user"></i> Profile -->
<!-- 					</a> -->
					<a class="dropdown-item" onClick="userLogout()">
						<i class="fa fa-lock"></i> Logout
					</a>
				</div>
			</li>
		</ul>
	</header>

	<!-- Edit Profile Modal -->
	<div class="modal fade" id="userProfile" tabindex="-1" role="dialog" aria-labelledby="userProfileLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title claim-num" id="userProfileLabel">Edit Profile</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">
							<img class="icon img-circle mb-3 mr-2" src="mainform/img/avatar.png" alt="Activities" width="150px">
							<input type='file' class="inputfile" />
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Username</span>
								</div>
								<input type="text" class="form-control" placeholder="jenniferlorem" disabled>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Name</span>
								</div>
								<input type="text" class="form-control" placeholder="${user_id}">
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Email Address</span>
								</div>
								<input type="email" class="form-control" placeholder="jennifer.lorem@mail.com">
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Old Password</span>
								</div>
								<input type="password" class="form-control">
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">New Password</span>
								</div>
								<input type="password" class="form-control">
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Confirm New Password</span>
								</div>
								<input type="password" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" onClick="profileOK()" data-dismiss="modal">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL -->