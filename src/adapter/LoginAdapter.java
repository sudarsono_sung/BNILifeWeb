package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

public class LoginAdapter {
    public static boolean ValidasiLogin(String lUserId, String lPassword) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String Query = "SELECT COUNT(1) count_row FROM ms_user_cms WHERE username = COALESCE(?, username) AND password = COALESCE(?, password) AND is_active = 1";
	CachedRowSet crs = null;
	boolean lCheck = false;
	try {
	    if (lUserId != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", lUserId));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }

	    if (lPassword != null) {
		listParam.add(database.QueryExecuteAdapter.QueryParam("string", lPassword));
	    } else {
		listParam.add(database.QueryExecuteAdapter.QueryParam("null", java.sql.Types.NULL));
	    }

	    crs = database.QueryExecuteAdapter.QueryExecute(Query, listParam, functionName);
	    if (crs.next() != false) {
		if (crs.getInt("count_row") > 0) {
		    lCheck = true;
		} else {
		    lCheck = false;
		}
	    }

	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, Query, lUserId);
	}
	return lCheck;
    }

}
