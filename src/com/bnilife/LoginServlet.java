package com.bnilife;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LoginAdapter;
import adapter.UserAdapter;

@WebServlet(urlPatterns = { "/login" }, name = "login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/login.jsp");
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lUserName = request.getParameter("username");  
		String lPassword = request.getParameter("password"); 
		String btnLogin = request.getParameter("btnLogin");
		try {
			lPassword = adapter.EncryptAdapter.encrypt(lPassword);
			
			if (btnLogin != null){
				if(LoginAdapter.ValidasiLogin(lUserName, lPassword) == true) {
				    	String role_id = UserAdapter.GetUserRole(lUserName);
					HttpSession session = request.getSession();
					session.setAttribute("user_id", lUserName);
					session.setAttribute("role_id", role_id);
					session.setMaxInactiveInterval(30*60);
					response.sendRedirect(request.getContextPath() + "/dashboard");
				}
				else {
					request.setAttribute("condition", "1");
					response.sendRedirect(request.getContextPath() + "/login");
//					RequestDispatcher rd = request.getRequestDispatcher("/login");
//					rd.forward(request,response);
				}
			}
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
