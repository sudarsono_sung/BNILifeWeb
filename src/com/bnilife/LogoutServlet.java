package com.bnilife;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns={"/logout"} , name="logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public LogoutServlet() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession(false);
		if(session != null){
			session.invalidate();
		}
		RequestDispatcher rd=request.getRequestDispatcher("/mainform/login.jsp");
		rd.forward(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession(false);
		if(session != null){
			session.invalidate();
		}
		RequestDispatcher rd=request.getRequestDispatcher("/mainform/login.jsp");
		rd.forward(request,response);
	}
}