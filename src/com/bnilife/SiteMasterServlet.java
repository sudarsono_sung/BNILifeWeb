package com.bnilife;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import adapter.UserAdapter;

@WebServlet(urlPatterns = { "/site_master" }, name = "site_master")
public class SiteMasterServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public SiteMasterServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    // String[] list_user_area;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	String user_id = (String) session.getAttribute("user_id");
	// String role_id = (String) session.getAttribute("role_id");

	// Declare button
	String keyBtn = request.getParameter("key");
	if (keyBtn == null) {
	    keyBtn = new String("");
	}

	// if (keyBtn.equals("RestrictedMenu")){
	if (keyBtn.equals("AllowedMenu")) {
	    List<model.mdlMenu> MenuList = new ArrayList<model.mdlMenu>();
	    MenuList = UserAdapter.LoadAllowedMenuforAccess(user_id);

	    Gson gson = new Gson();

	    String jsonlistMenuID = gson.toJson(MenuList);

	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonlistMenuID);
	}
    }
}
