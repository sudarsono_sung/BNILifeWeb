package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import adapter.LogAdapter;

public class QueryExecuteAdapter {

	public static CachedRowSet QueryExecute(String sql, String function){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		RowSetFactory rowSetFactory = null;
		CachedRowSet crs = null;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			rs = pstm.executeQuery();
			rowSetFactory = RowSetProvider.newFactory();
			crs = rowSetFactory.createCachedRowSet();
			crs.populate(rs);
		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , "InventOauthAPIService");
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (rs != null) rs.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , "InventOauthAPIService");
			}
		}
		return crs;
	}

	public static CachedRowSet QueryExecute(String sql, List<model.Query.mdlQueryExecute> queryParam, String function){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		RowSetFactory rowSetFactory = null;
		CachedRowSet crs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			if (queryParam.size() >= 0){
				for (int i=1;i<=queryParam.size();i++){
					switch (queryParam.get(i-1).paramType.toLowerCase()) {
						case "string":
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						case "int":
							pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
							break;
						case "decimal":
						case "double":
							pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
							break;
						case "boolean":
							pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
							break;
						case "null":
							pstm.setNull(i, java.sql.Types.NULL);
						break;
						default: 
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
					}
				}
			}

			rs = pstm.executeQuery();
			rowSetFactory = RowSetProvider.newFactory();
			crs = rowSetFactory.createCachedRowSet();
			crs.populate(rs);
		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , "InventOauthAPIService");
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (rs != null) rs.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , "InventOauthAPIService");
			}
		}
		return crs;
	}

	public static Boolean QueryManipulate(String sql, List<model.Query.mdlQueryExecute> queryParam, String function){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			if (queryParam.size() >= 0){
				for (int i=1;i<=queryParam.size();i++){
					switch (queryParam.get(i-1).paramType.toLowerCase()) {
						case "string":
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						case "int":
							pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
							break;
						case "decimal":
						case "double":
							pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
							break;
						case "boolean":
							pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
							break;
						case "null":
							pstm.setNull(i, java.sql.Types.NULL);
						break;
						default: 
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
					}
				}
			}

			pstm.execute();
			success = true;
		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , "InventOauthAPIService");
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , "InventOauthAPIService");
			}
		}
		return success;
	}

	public static model.Query.mdlQueryExecute QueryParam(String type, Object value){
		model.Query.mdlQueryExecute param = new model.Query.mdlQueryExecute();
		param.paramType = type;
		param.paramValue = value;

		return param;
	}

	public static Boolean QueryTransaction(List<model.Query.mdlQueryTransaction> listMdlQueryTrans, String function){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;
		String sql;
		List<model.Query.mdlQueryExecute> queryParam;

		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);

			for(model.Query.mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans){
				pstm = null;
				sql = mdlQueryTrans.sql;
				queryParam = new ArrayList<model.Query.mdlQueryExecute>();
				queryParam.addAll(mdlQueryTrans.listParam);
				pstm = connection.prepareStatement(sql);

				for (int i=1;i<=queryParam.size();i++){
					switch (queryParam.get(i-1).paramType.toLowerCase()) {
						case "string":
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						case "int":
							pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
							break;
						case "decimal":
						case "double":
							pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
							break;
						case "boolean":
							pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
							break;
						case "null":
							pstm.setNull(i, java.sql.Types.NULL);
						break;
						default: 
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
					}
				}
				pstm.executeUpdate();
			}

			connection.commit(); //commit transaction if all of the proccess is running well
			success = true;
		}catch(Exception ex){
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "Transaction"+function, pstm.toString() , "InventOauthAPIService");
		            }
		        }
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , "InventOauthAPIService");
			success = false;
		}finally{
			try{
				if (pstm != null) pstm.close();
				connection.setAutoCommit(true);
				if (connection != null) connection.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , "InventOauthAPIService");
			}
		}

		return success;
	}

}


